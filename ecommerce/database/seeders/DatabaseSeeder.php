<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //  User::factory(10)->create();

        
        //  DB::table('categories')->insert([
        //      'title'=>'polo',
        //      'description'=>'text description'
        //  ]);

    //     Category::create([
    //         'title'=>'t-shirt',
    //         'description'=>'t-shirt description'
    //    ] );

        //  Product::factory(100)->create();

        $this->call([
           ProductTableSeeder::class
        ]);



    }
}
