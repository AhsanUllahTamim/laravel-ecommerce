<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\productController;
use App\Http\Controllers\task1controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//update: PUT/PATCH
//delete: DELETE


// Route::get('/home', function () {
//     return view('home');
// });


// route::group(['prefix'=>'products'],function(){

//     route::get('/contact',[productController::class,'contact'])->name('product.contact');
//     route::get('/add',[productController::class,'add'])->name('product.add');


// });

//  route::get('product/contact',[productController::class,'contact'])->name('product.contact');
//  route::get('product/add',[productController::class,'add'])->name('product.add');
//  route::get('/products/home',[productController::class,'home'])->name('product.home');

//  route::get('/products/show',[productController::class,'show'])->name('product.show');
//  route::get('/products',[productController::class,'index'])->name('product.index');

//  route::get('/products/{id}',function($id){

//      $product_name='t-shirt';
//     return view('products.show',compact('id','product_name'));
//     // return 'show'.$id;

//  })->name('products.show');

//   route::get('/products/{id}',[productController::class,'show'])->name('product.show');


// Route::get('/login', function () {
//     return view('login');
// });


route::prefix('admin')->middleware('auth')->group (function(){
Route::get('/deleted-products', [productController::class,'trush'])->name('products.trush');
Route::get('/deleted-products/{id}/restore', [productController::class,'restore'])->name('products.restore');
Route::get('/products/download-pdf', [productController::class,'pdf'])->name('products.pdf');
Route::delete('/deleted-products/{id}', [productController::class,'delete'])->name('products.delete');

// Route::get('/products', [productController::class,'index'])->name('products.index');
// Route::get('/products/create', [productController::class,'create'])->name('products.create');
// Route::post('/products/store', [productController::class,'store'])->name('products.store');
// Route::get('/products/{product}', [productController::class,'show'])->name('products.show');
// Route::get('/products/{product}/edit', [productController::class,'edit'])->name('products.edit');
// Route::patch('/products/{product}', [productController::class,'update'])->name('products.update');
// Route::delete('/products/{product}product', [productController::class,'destroy'])->name('products.destroy');

route::resource('products',productController::class)->middleware('auth');

});

require __DIR__.'/auth.php';


// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');



Route::get('/dashboard', function () {
    return view('backend.dashboard');
})->middleware('CheckAgeMiddleware');

Route::get('/{categoryId?}',[FrontendController::class, 'welcome'])->name('welcome');


Route::get('/table', function () {
    return view('backend.table');
});




