<x-backend.layouts.master>


<div class="container-fluid px-4">
        <h1 class="mt-4">Products</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Tables</li>
        </ol>

        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                product Updated
                <a href="{{route('products.index')}}" class="btn btn-sm btn-primary">list</a>
            </div>
            <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <form action="{{ route('products.update',['product'=>$product->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')

                    <div class="mb-3">
                        <label for="category_id" class="form-label">Category</label>
                        <select name="category_id" class="form-control" id="category_id">
                            <option value="">Select One</option>
                            @foreach($categories as $key=>$category)
                        <option value="{{$key}}"  {{$product->category_id == $key ? 'selected' : ''}}>{{$category}}</option> 
                        @endforeach                
                        </select>                    
                        @error('category_id')
                        <div  class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="title" class="form-label">Title</label>
                        <input name="title" type="text" class="form-control" id="title" value="{{ old('title', $product->title)}}">
                        
                        @error('title')

                        <div  class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input name="price" type="text" class="form-control" id="price" value="{{ $product->price}}">
                        @error('price')

                        <div  class="text-danger">{{ $message }}</div>
                        @enderror

                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <input name="description" type="text" class="form-control" id="description" value="{{ $product->description}}">

                        @error('description')

                        <div  class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Image</label>
                        <input name="image" type="file" class="form-control" id="image">
    
                        @error('description')
    
                        <div  class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                
                
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>


</x-backend.layouts.master>



