<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;



     protected $table = 'products';
     protected $fillable = ['title','price','description','image','category_id','created_by',
    'updated_by','deleted_by'];

     public function category(){
         return $this->belongsTo(category::class);

     }
     public function createdBy(){
         return $this->belongsTo(User::class ,'created_by');  

     }
}

