<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function welcome($catecoryId =null)
    {

        $categories= Category::pluck('title','id')->toarray();
        if($catecoryId){

        //     $category =Category::findorfail($catecoryId);
        // $products =$category->products;
            $products = Product::where('category_id',$catecoryId)->paginate(9);
        }
        else{
            $products = Product::paginate(9);
        }

   // dd($categories);
        return view('welcome', compact('products','categories'));


    }
}
