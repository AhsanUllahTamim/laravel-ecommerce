<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
// use Faker\Provider\Image;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Echo_;
use Image;
// use Barryvdh\DomPDF\Facade\Pdf;
use PDF;



class productController extends Controller
{
    function contact()
    {

        return view('contact');



    }

    function home()
    {

       echo "controller home";



    }
    function edit($id)
    {
        $categories= Category::pluck('title','id')->toarray();
        $product= Product::findorfail($id);
        
        return view('backend.products.edit', compact('product','categories'));   

    }
    function update(ProductRequest $request ,$id)
    {
        try
        {

            $requestdata=$request->all();
            $product= Product::findorfail($id);
            if($request->hasFile('image')){
                $file=$request->File('image');
                // dd($file->getclientOriginalExtension());
                $filename= time() . '.' . $file->getclientOriginalExtension();                   
                $image= Image::make($request->file('image'))
                ->resize(300,200)
                ->save(storage_path().'/app/public/products/'.$filename);
                $requestdata['image']=$filename; 
            }else
            {
                $requestdata['image']= $product->image;
            }
            $requestdata['updated_by']= Auth()->user()->id;

             $product->update($requestdata);
             return redirect()->route('products.index')->withMessage('successfully updated !');
            
    
        }
        catch(QueryException $e ){     
            return redirect()->back()->withInput()->withErrors($e->getMessage());
               //  dd($e->getMessage());
       }
    }
    public function show(Product $product)
    {

        // dd($product-> createdBy);
        // $product = Product::where('id', '=', $id)->first();
        // $product= Product::findorfail($product);

        // dd($product->category);

        //  return view('backend.products.show'[
        //      'product' => $product
        //  ]); 
         return view('backend.products.show' , compact('product'));


    }

    public function index()
    {
        // $products=Product::all();
        $products=Product::orderBy('id','desc')->get();
        return view('backend.products.index', compact('products'));


    }

    public function create()

    {

        $categories= Category::pluck('title','id')->toarray();
        // dd($categories);
        return view('backend.products.create' , compact('categories'));


    }

    public function store(ProductRequest $request)
    {
        try{
            $requestdata=$request->all();
            if($request->hasFile('image')){
                $file=$request->File('image');
                // dd($file->getclientOriginalExtension());
                $filename= time() . '.' . $file->getclientOriginalExtension();                   
                $image= Image::make($request->file('image'))
                ->resize(300,200)
                ->save(storage_path().'/app/public/products/'.$filename);
                $requestdata['image']=$filename; 
            }

            //  dd($requestdata);

            //  $request->validate([
            //     'title' => 'required|min:4|unique:products,title',
            //     // 'price' => 'required|numeric',
            // ]);

            // dd('request validated');

            // $postdata=$request->all();
            

            // $product = new Product();
            // $product->title = $request->title;
            // $product->price = $request->price;
            // $product->description = $request->description;
            // $product->save();

            // DB::table('products')->insert([

            //     'title'=>$request->title,
            //     'price'=>$request->price,
            //     'description'=>$request->description,
            // ]);

            // dd($requestdata);
            $requestdata['created_by']= Auth()->user()->id;
            
            Product::create($requestdata);


            // Session::flush('message', 'successfully saved ');

            // dd($postdata);
            // Product::create([
    
            //     'title'=>$postdata['title'],
            //     'price'=>$postdata['price'],
            //     'description'=>$postdata['description']
            // ]);
            return redirect()->route('products.index')->withMessage('successfully saved !');
        }
        catch(QueryException $e ){
       
             return redirect()->back()->withInput()->withErrors($e->getMessage());
                //  dd($e->getMessage());
        }

    }
    public function destroy(Product $product)
    {
        $product->update(['deleted_by'=> Auth()->id()]); 
        // dd( $product);

    //   $product= Product::findorfail($id);
      $product->delete();
      return redirect()->route('products.index')->withMessage('successfully deleded !');


    }

    public function trush()
    {
      $products= Product::onlyTrashed()->get();

      return view('backend.products.trush',compact('products'));


    }
    public function restore($id)
    {
       Product::withTrashed()->where('id',$id)->restore(); 
        return redirect()->route('products.trush')->withMessage('successfully restore');
    }
    public function delete($id)
    {
       Product::withTrashed()
           ->where('id',$id)
           ->forceDelete(); 
        return redirect()->route('products.trush')->withMessage('successfully deleted');
    }
    public function pdf(){
        $products= Product::latest()->get();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('invoice.pdf');
    }

 









}
