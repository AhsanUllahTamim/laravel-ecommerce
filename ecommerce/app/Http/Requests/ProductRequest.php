<?php

namespace App\Http\Requests;

use FontLib\Table\Type\post;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {


        $imageValidateRules='mimes:png,jpg|min:5|max:2024';
        if ($this->isMethod('post')){
            $imageValidateRules='required|mimes:png,jpg|min:5|max:2024';  
        }

        return [
            'title' => 'required|min:4|max:10|unique:products,title',
            'image' => $imageValidateRules,
            'category_id' =>'required|exists:categories,id',
        ];
    }
}
